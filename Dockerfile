FROM debian:buster-slim

SHELL ["/bin/bash", "-xc"]

COPY  texlive-install.sh \
      texlive-install.sh

COPY  docker-layer-cleanup.sh \
      docker-layer-cleanup.sh

RUN \
  apt-get update -yq && \
  bash texlive-install.sh && \
  bash docker-layer-cleanup.sh && \
  :
