texlive_image_tag = pedroarthur/texlive-debian-amd64:latest

.PHONY: docker.build

docker.build:
	docker build -t $(texlive_image_tag) .
