#!/bin/bash

should_clear=${CLEAR_DOCKER_LAYER:-true}

[[ $should_clear == "true" ]] || exit 0

xargs -tl apt-get -yq <<EoF
  autoremove
  autopurge
  autoclean
  clean
EoF

directories_to_clean=(
  /var/cache/apt
  /var/lib/apt/lists
  /var/log/apt
)

find "${directories_to_clean[@]}" -mindepth 1 -delete

files_to_remove=(
  /var/log/dpkg.log
)

rm "${files_to_remove[@]}" || true
