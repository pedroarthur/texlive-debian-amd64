Build
===

    make docker.build

Run
===

    bash texlive-docker.sh pdflatex aquivo.tex
