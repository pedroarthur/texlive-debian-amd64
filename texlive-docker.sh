#!/bin/bash

docker_run_args=(
  --interactive
  --tty
  --rm

  # expose PWD to runtime
  --volume "$PWD:$PWD:rw"
  --workdir "$PWD"

  # run as if the current user
  --user "$(id -u):$(id -g)"

  "${docker_image:=pedroarthur/texlive-debian-amd64:latest}"
)

docker run "${docker_run_args[@]}" "${@}"
