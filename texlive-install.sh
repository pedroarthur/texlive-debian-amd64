#!/bin/bash

packages=(
  tex-gyre

  texlive-base
  texlive-bibtex-extra
  texlive-binaries
  texlive-fonts-extra
  texlive-generic-recommended
  texlive-lang-english
  texlive-lang-portuguese
  texlive-latex-base
  texlive-latex-recommended
  texlive-publishers
  texlive-science
  texlive-xetex
)

apt_install_args=(
  -yq
  --no-install-recommends
)

apt-get install "${apt_install_args[@]}" "${packages[@]}"
